/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import conexion.Conexion;
import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "Productos", urlPatterns = {"/Productos"})
public class Productos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        /* TODO output your page here. You may use following sample code. */
        out.println("<!DOCTYPE html>" + "<html>" + "<nav>" + "<link rel='stylesheet' type='text/css' media='screen' href='css/Style.css'>" + "<title>Servlet Productos</title>" + "</nav>" + "<body>"+"<header >Interesado en comprar?</header>"
                + "<p align=\"center\">" + "<a href=\"ArribaProducto.html\"> <img src=\"iconos/icons8-subir-100.png\" width=\"60\" height=\"60\"></a>"+"<p><--Este anda.</p>"
                + "</p>"+ "<p align=\"center\" >" + "<a href=\"CambiaProducto\"> <img src=\"iconos/icons8-editar-100.png\" width=\"60\" height=\"60\"></a>" +"<p><--Este esta de adorno.</p>"+ "</p>" +"<p align=\"center\" >" + "<a href=\"BajaProducto\"> <img src=\"iconos/icons8-eliminar-100.png\" width=\"60\" height=\"60\"></a>" + "</p>" +"<p><--Este esta de adorno.</p>"
                
                + "<main>");
        String consulta = "SELECT * FROM productos";
        Connection con = null;
        try {

            con = DB.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Producto miProducto = new Producto();
                miProducto.Nombre = rs.getString("Nombre");
                miProducto.Descripcion = rs.getString("Descripcion");
                miProducto.Unidades = rs.getInt("Unidades");
                miProducto.Precio = rs.getInt("Precio");
                miProducto.Foto = rs.getString("Foto");

                out.println("<article >");
                out.println("<img src='" + miProducto.Foto + "' width='200' />");
                out.println("</div> Nombre: " + miProducto.Nombre + "</div>");
                out.println("<div>Descripcion: " + miProducto.Descripcion + "</div>");
                out.println("<div>Stock:" + miProducto.Unidades + "</div>");
                out.println("<div>Precio: " + miProducto.Precio + "</div>");

                out.println("</article>");
            }

        } catch (ClassNotFoundException ex) {
            out.print("Error" + ex);
        } catch (SQLException ex) {
            out.print("Error" + ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }

            } catch (SQLException quePaso) {
                System.out.println("Error SQL2: " + quePaso);
            }
        }
        out.println("</main>\n" + "</body>\n" + "</html>\n");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
