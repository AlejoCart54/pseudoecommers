-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2020 a las 23:02:34
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pseudocomercio`
--
CREATE DATABASE IF NOT EXISTS `pseudocomercio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pseudocomercio`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `Nombre` varchar(40) NOT NULL,
  `Unidades` int(100) NOT NULL,
  `Foto` varchar(255) NOT NULL,
  `Descripcion` varchar(75) NOT NULL,
  `Precio` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`Nombre`, `Unidades`, `Foto`, `Descripcion`, `Precio`) VALUES
('Radio', 20, 'https://www.radiotronica.es/media/catalog/product/cache/8/image/600x600/e07be0874a35df62acae008fc4d0993a/m/o/motorola-sl4000e-front_1.jpg', 'Radio compacta del 99', 700),
('Pc Predator', 50, 'https://cdn.comparez-malin.fr/img/acer/2019/17268/acer-predator-helios-300-ph315-52-2019-3.jpg', 'Computadora marca predator, ideal para jugar.', 45000),
('Pc Vintage', 3, 'https://tse1.mm.bing.net/th?id=OIP.OqrdJLsdKIQQT4WuXJHpaQHaJ4&pid=Api&P=0&w=300&h=300', 'Computadora vintage, imitacion de pc antiguo', 70000),
('Estereo Panasonic', 100, 'https://http2.mlstatic.com/panasonic-estereo-D_NQ_NP_957318-MLM26581120045_122017-F.jpg', 'Estereo panasonic ultimo modelo', 25000);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
